package br.com.senac.calculodesalario;

public class CalculadoraDeSalario {

    public double calcular(Funcionario funcionario) {

        if (funcionario.getCargo().equals(Cargo.DESENVOLVEDOR)) {

            if (funcionario.getSalario() > 3000) {
                return 5000 * 0.8;
            } else {
                return 1000 * 0.9;
            }
        }


        if (funcionario.getSalario() > 2500) {
            return 3000 * 0.75 ; 
        }

        return 1500 * 0.85;

    }

}
